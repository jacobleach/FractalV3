package org.sythe.suf.fractal.view;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.sythe.suf.fractal.components.FractalDisplay;
import org.sythe.suf.fractal.components.IndexedButton;
import org.sythe.suf.fractal.components.RuleDisplay;
import org.sythe.suf.fractal.model.FractalSquare;
import org.sythe.suf.fractal.presenter.IFractalPresenter;

public class FractalView extends JFrame implements IFractalView
{
	private IFractalPresenter presenter;

	private final int width = 788;
	private final int height = 552;

	private FractalDisplay fractalDisplay;
	private JSlider generationSlider;
	private JPanel pan1, pan2, pan3, pan4, pan5;
	private JLabel sliderLabel;
	private IndexedButton[] ruleButtons;
	private JButton[] ruleChangers;
	private int selectedRule;
	
	private RuleDisplay ruleDisplay;

	public FractalView(IFractalPresenter presenter)
	{
		this.presenter = presenter;
		initComponents();
	}

	/*
	 * ************************** Component Initialization Methods **************************
	 */

	private void initComponents()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);

		loadPanels();

		initFractalDisplay();
		initSlider();
		// This sucks. It needs to be a component anyways.
		ruleButtons = new IndexedButton[15];
		initRuleButtons(5, 3, 50, 2, pan5, ruleButtons);
		initRuleChanger();

		// Must be done last - after all the other components and their borders are created and
		// added
		Insets insets = this.getInsets();
		this.setSize(insets.left + insets.right + width, insets.top + insets.bottom + height);
	}

	private void initRuleChanger()
	{
		ruleDisplay = new RuleDisplay();
		pan2.add(ruleDisplay);
	}

	private void initRuleButtons(int columns, int rows, int buttonSize, int padding, JPanel panel, IndexedButton[] buttons)
	{
		// buttons = new IndexedButton[columns * rows];
		int row = -1;// Needs to be -1 because it incriments right away in the first loop
		int column;
		for (int i = 0; i < columns * rows; i++)
		{
			column = (i % columns);
			if (column == 0)
			{
				row++;
			}
			buttons[i] = new IndexedButton(i, presenter.getRuleColor(i));
			buttons[i].setSize(buttonSize, buttonSize);
			buttons[i].setLocation((column * buttonSize) + padding + column, (row * buttonSize) + padding + row);
			buttons[i].addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					IndexedButton source = (IndexedButton) e.getSource();
					presenter.clickedRuleSelector(source.getIndex());
				}
			});
			panel.add(buttons[i]);
		}
	}

	private void loadPanels()
	{
		pan1 = new JPanel();
		pan1.setSize(514, 514);
		pan1.setLocation(0, 0);
		pan1.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		pan1.setLayout(null);
		this.add(pan1);

		pan2 = new JPanel();
		pan2.setSize(258, 258);
		pan2.setLocation(514, 256);
		pan2.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		pan2.setLayout(null);
		this.add(pan2);

		pan3 = new JPanel();
		pan3.setSize(258, 50);
		pan3.setLocation(514, 206);
		pan3.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		pan3.setLayout(null);
		this.add(pan3);

		pan4 = new JPanel();
		pan4.setSize(258, 50);
		pan4.setLocation(514, 156);
		pan4.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		pan4.setLayout(null);
		this.add(pan4);

		pan5 = new JPanel();
		pan5.setSize(258, 156);
		pan5.setLocation(514, 0);
		pan5.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		pan5.setLayout(null);
		this.add(pan5);
	}

	private void initFractalDisplay()
	{
		fractalDisplay = new FractalDisplay(512);
		fractalDisplay.setLocation(1, 1);
		pan1.add(fractalDisplay);
	}

	private void initSlider()
	{
		sliderLabel = new JLabel("Iterations:");
		sliderLabel.setLocation(3, 1);
		sliderLabel.setSize(66, 40);

		generationSlider = new JSlider(0, 9);
		generationSlider.setPaintLabels(true);
		generationSlider.setSnapToTicks(true);
		generationSlider.setSize(185, 40);
		generationSlider.setLabelTable(generationSlider.createStandardLabels(1));
		generationSlider.setLocation(68, 5);
		generationSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(ChangeEvent e)
			{
				presenter.generationChanged(generationSlider.getValue());
			}
		});

		pan3.add(sliderLabel);
		pan3.add(generationSlider);
	}

	public void setGenerationSlider(int generation)
	{
		generationSlider.setValue(generation);
	}

	public void setFractalImage(FractalSquare[] fractal)
	{
		fractalDisplay.paintFractal(fractal);
	}

	public void setRuleDisplay(Color[] colors)
	{
		if (colors.length >= ruleChangers.length)
		{
			for (int i = 0; i < ruleChangers.length; i++)
			{
				ruleChangers[i].setBackground(colors[i]);
			}
		}
		else
		{
			// add something
		}
	}

	public IFractalPresenter getPresenter()
	{
		return presenter;
	}

	public void setPresenter(IFractalPresenter fractalPresenter)
	{
		presenter = fractalPresenter;
	}

	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
	}

	public JFrame getFrame()
	{
		return this;
	}

	public void setRule(int index, Color color)
	{
		if (index < ruleChangers.length)
		{
			ruleChangers[index].setBackground(color);
		}
		else
		{
			// Do something
		}
	}

	public int getSelectedRule()
	{
		return selectedRule;
	}

	public void setSelectedRule(int index)
	{
		if (index < ruleButtons.length)
		{
			selectedRule = index;
		}
		else
		{
			// do something
		}
	}
}
