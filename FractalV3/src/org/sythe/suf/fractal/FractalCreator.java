package org.sythe.suf.fractal;

import javax.swing.SwingUtilities;
import org.sythe.suf.fractal.model.Fractal;
import org.sythe.suf.fractal.presenter.FractalPresenter;
import org.sythe.suf.fractal.view.FractalView;

public class FractalCreator
{
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                Fractal a = new Fractal(15, 9);
                FractalPresenter b = new FractalPresenter();
                b.setModel(a);
                FractalView c = new FractalView(b);
                b.setView(c);
                b.run();
            }
        });
        
    }

}
