package org.sythe.suf.fractal.model;

public class FractalRuleDoesNotExistException extends RuntimeException
{
    public FractalRuleDoesNotExistException(String message)
    {
        super(message);
    }
}
