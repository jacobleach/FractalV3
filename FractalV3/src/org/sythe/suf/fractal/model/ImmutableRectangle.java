package org.sythe.suf.fractal.model;

import java.awt.geom.Rectangle2D;

public class ImmutableRectangle extends Rectangle2D
{	
	public final int x;
	public final int y;
	public final int width;
	public final int height;
	
	public ImmutableRectangle(int x, int y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public Rectangle2D createIntersection(Rectangle2D r)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Rectangle2D createUnion(Rectangle2D r)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int outcode(double x, double y)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setRect(double x, double y, double w, double h)
	{
		//Do nothing
	}

	@Override
	public double getHeight()
	{
		return height;
	}

	@Override
	public double getWidth()
	{
		return width;
	}

	@Override
	public double getX()
	{
		return x;
	}

	@Override
	public double getY()
	{
		return y;
	}

	@Override
	public boolean isEmpty()
	{
		return width == 0 && height == 0;
	}

}
