package org.sythe.suf.fractal.model;

import java.awt.Color;


public interface IFractalModel
{
    int[] getRule(int index);
    int getNumberOfRules();
    FractalSquare[] getFractalSquares();
    int getGeneration();
    int getDisplaySize();
    Color getRuleColor(int index);
    Color[] getRuleAsColors(int index);
    
    void setRule(int index, int fractalIndex, int rule);
    void setRule(int index, int[] rule);
    void setGeneration(int generation);
    void setRuleColor(int index, Color c);
    
    void reIterate();
    
}
