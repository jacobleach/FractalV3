package org.sythe.suf.fractal.model;

import java.awt.Color;

public class FractalRule
{
	private Color ruleColor;
	private FractalRule[] fractalRule;

	/*
	 * copy holds the same information as fractalRule and is used to
	 * return when getRule() is called. This protects fractalRule
	 * and since getRule() is called a lot simply saving the 1 or 4
	 * sized array saves a lot of time (2-4ms per 9 generation iteration)
	 * so it is worth the bit of extra memory it uses.
	 */
	private FractalRule[] copy;
	private int index;

	/*
	 * Initializes color and sets the rule to itself
	 */
	protected FractalRule(Color color, int index)
	{
		this.ruleColor = color;
		this.index = index;
		setRule(new FractalRule[] { this });
	}

	// ****************Setter Methods****************

	/*
	 * Sets the rule by creating a new array of length 4 or 1.
	 * If its length 1, it doesn't split.
	 * If its length 4, it does
	 */
	protected void setRule(FractalRule[] rule)
	{
		int size = (rule.length >= 4) ? 4 : 1;
		fractalRule = new FractalRule[size];

		for (int i = 0; i < size; i++)
		{
			fractalRule[i] = rule[i];
		}

		copy = fractalRule.clone();
	}

	protected void setRule(int index, FractalRule rule)
	{
		try
		{
			fractalRule[index] = rule;
			copy = fractalRule.clone();
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new FractalRuleDoesNotExistException(
					"Tried to access a FractalRule that does not exist. Perhaps you tried to set a specific FractalRule of a Fractal rule with an index that is > 4 or if not split, > 1.");
		}
	}

	/**
	 * Sets the color for this rule.
	 * 
	 * @param color
	 *            the color that the rule should be displayed as
	 */
	protected void setColor(Color color)
	{
		this.ruleColor = color;
	}

	// ****************Getter Methods****************

	/*
	 * Returns the rule's index
	 */
	protected int getIndex()
	{
		return index;
	}

	/*
	 * Returns the color associated with this rule
	 */
	protected Color getColor()
	{
		return ruleColor;
	}

	/*
	 * Returns a copy of fractalRule so that the rules can be seen but the array itself can not
	 * be modified except through the setRule method
	 */
	protected FractalRule[] getRule()
	{
		return copy;
	}

	/*
	 * Returns the rule based on their indexes
	 */
	protected int[] getRuleIndexes()
	{
		int[] temp = new int[fractalRule.length];

		for (int i = 0; i < fractalRule.length; i++)
		{
			temp[i] = fractalRule[i].index;
		}

		return temp;
	}

	/**
	 * Returns an array of Color objects representing this rule
	 * 
	 * @return an array of Color objects representing this rule
	 */
	protected Color[] getRuleAsColors()
	{
		Color[] temp = new Color[fractalRule.length];
		for (int i = 0; i < temp.length; i++)
		{
			temp[i] = fractalRule[i].getColor();
		}

		return temp;
	}
}