package org.sythe.suf.fractal.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.sythe.suf.fractal.model.FractalRule;

public class RuleSelectionDialogue extends JDialog implements ActionListener
{
	private int answer;

	private RuleSelectionDialogue(JFrame owner, Color[] colors)
	{
		super(owner, "Choose a color:", true);
		createWindow(colors);
		setLocationRelativeTo(owner);
		setVisible(true);
	}

	private void initRuleButtons(int columns, int rows, int buttonSize, int padding, JPanel panel, Color[] colors)
	{
		IndexedButton[] buttons = new IndexedButton[columns * rows];
		int row = -1;// Needs to be -1 because it incriments right away in the first loop
		int column;
		for (int i = 0; i < columns * rows; i++)
		{
			column = (i % columns);
			if (column == 0)
			{
				row++;
			}
			buttons[i] = new IndexedButton(i, colors[i]);
			buttons[i].setSize(buttonSize, buttonSize);
			buttons[i].setLocation((column * buttonSize) + padding + column, (row * buttonSize) + padding + row);
			buttons[i].addActionListener(this);
			panel.add(buttons[i]);
		}
	}

	private void createWindow(Color[] colors)
	{
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(258, 156));
		getContentPane().add(panel);
		setResizable(false);
		initRuleButtons(5, 3, 50, 2, panel, colors);
		pack();
	}

	public void actionPerformed(ActionEvent e)
	{
		IndexedButton button = (IndexedButton) e.getSource();
		answer = button.getIndex();
		setVisible(false);
	}

	private int getAnswer()
	{
		return answer;
	}

	public static int showDialog(JFrame owner, Color[] colors)
	{
		RuleSelectionDialogue temp = new RuleSelectionDialogue(owner, colors);
		return temp.getAnswer();
	}

}
