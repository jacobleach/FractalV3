package org.sythe.suf.fractal.components;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class RuleDisplay extends JPanel
{
	private IndexedButton[] ruleChangers;

	public RuleDisplay()
	{
		this.setLocation(1, 1);
		this.setSize(258, 258);
		this.setLayout(null);
		initRuleDisplay();
	}

	private void initRuleDisplay()
	{
		int row = -1;
		int buttonSize = 126;
		int padding = 2;
		ruleChangers = new IndexedButton[4];

		for (int i = 0; i < 4; i++)
		{
			int column = (i % 2);
			if (column == 0)
			{
				row++;
			}

			ruleChangers[i] = new IndexedButton(i);
			ruleChangers[i].setSize(buttonSize, buttonSize);
			ruleChangers[i].setLocation((column * buttonSize) + padding + column, (row * buttonSize) + padding + row);

			final Component parent = this;

			ruleChangers[i].addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					parent.dispatchEvent(e);
				}
			});
			this.add(ruleChangers[i]);
		}
	}
}
