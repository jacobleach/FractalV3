package org.sythe.suf.fractal.components;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Test;
import org.sythe.suf.fractal.components.ButtonGrid;

public class ButtonGridTest
{

	@Test
	public void testCalculateLocation()
	{
		ButtonGrid test = new ButtonGrid(10, 10);
		
		Point point = test.getLocation(0, 0);
		
		assertEquals("First button x != 0", 0, point.x);
		assertEquals("First button y != 0", 0, point.y);
		
		test = new ButtonGrid(1, 1);

		assertEquals(52, test.getPerfectSize().width);
	}

}
