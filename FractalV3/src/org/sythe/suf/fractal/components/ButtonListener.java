package org.sythe.suf.fractal.components;

import java.awt.event.ActionEvent;

public interface ButtonListener
{
	public void buttonEvent(ActionEvent e, int index);
}
