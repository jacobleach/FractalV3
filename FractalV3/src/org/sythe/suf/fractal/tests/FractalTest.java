package org.sythe.suf.fractal.tests;

import static org.junit.Assert.*;
import org.junit.Test;
import org.sythe.suf.fractal.model.*;

public class FractalTest
{

	@Test(expected = FractalRuleDoesNotExistException.class)
	public void testRuleFractalRuleDoesNotExistException()
	{
		Fractal f = new Fractal(12, 9);
		f.setRule(15, new int[] { 1 });
		f.setRule(1, new int[] { 15 });
		f.getRule(13);
		f.getRuleColor(13);
	}

	@Test
	public void testGetRuleColor()
	{
		Fractal f = new Fractal(12, 9);
		assertNotNull("getRuleColor returned null", f.getRuleColor(1));
	}

	@Test
	public void testMoveToFront()
	{
		int sizeOfTest = 10;

		for (int j = 0; j <= sizeOfTest; j++)
		{
			Integer[] test = new Integer[sizeOfTest];

			System.out.println("Test: " + j + "\n");

			for (int i = 0; i < j; i++)
			{
				test[test.length - i - 1] = i;
			}

			for (int i = 0; i < test.length; i++)
			{
				System.out.print(test[i] + ", ");
			}

			Fractal.moveToFront(test, j);

			System.out.println("\n\n\n----------------------------------------------\n\n");

			for (int i = 0; i < test.length; i++)
			{
				System.out.print(test[i] + ", ");
			}

			for (int i = 0; i < j; i++)
			{
				assertNotNull("moveToFront failed. size = " + j, test[i]);
			}

			System.out.println("\n\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
		}

	}

}
