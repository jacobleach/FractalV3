FractalV3
=========
A program used to create geometric fractals out of squares. 

This is the third iteration of the program. The first iteration was written during December 2010 while I was a senior in highschool. It required you to hard code the fractal into an array, used a massive string of if else's to produce the fractal, and wasn't interactive at all. The second iteration was written during December 2011 while I was a freshmen at Marist. It added the GUI that is present in version three as well as the current method for iterating the fractal.

The third iteration is a nearly complete rewrite of the second. It attempts to use a MVP pattern to seperate the GUI from the model of the fractal. The second version was awful in this regard and I found it hard to be able to control the changes needed in the fractal from all the parts of the GUI. In general this problem has been solved in the third iteration due to the MVP pattern. The code is also much nicer in the third version, more stable, faster, and more commented.

The main motivators for rewriting the program essentially from scratch were:

1) A display performance bug that with the awful GUI code was too annoying to fix

2) A desire to create better looking and maintainable code

3) A desire to take the project to the next level of correctness and learn while doing it
